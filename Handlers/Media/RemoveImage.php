<?php

namespace Lingua\Handlers\Media;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class RemoveImage implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("storageDir")
     */
    private $StorageDir;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $productId = $request->getAttribute('productId');
        $slot = $request->getAttribute('slot');
        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        // TODO: Check for permissions

        if (!$this->ValidationPresetsBuilder
                ->mongoidstring()
                ->validate($productId)
            ) {
            return $this->ResponseBuilder
                ->error()
                ->message('Invalid productId')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $product = $this->Mongo->products->findOne([
            '_id' => new \MongoDB\BSON\ObjectId($productId)
        ]);

        if (!$product || !is_dir($this->StorageDir.'/'.$productId)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Pointed product does not exists')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $finalFilePaths = glob($this->StorageDir.'/'.$productId.'/'."$slot.*");
        if ($finalFilePaths && is_array($finalFilePaths) && is_file($finalFilePaths[0])) {
            unlink($finalFilePaths[0]);
        }

        return $this->ResponseBuilder
            ->success()
            ->build();

        
    }
}