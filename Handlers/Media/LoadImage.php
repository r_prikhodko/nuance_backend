<?php

namespace Lingua\Handlers\Media;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class LoadImage implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("storageDir")
     */
    private $StorageDir;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $files = $request->getUploadedFiles();
        $productId = $request->getAttribute('productId');
        $slot = $request->getAttribute('slot');
        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        // TODO: Check for permissions

        if (!$this->ValidationPresetsBuilder
                ->mongoidstring()
                ->validate($productId)
            ) {
            return $this->ResponseBuilder
                ->error()
                ->message('Invalid productId')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        // Check is image loaded
        if (!$files) {
            return $this->ResponseBuilder
                ->error()
                ->message('File not present in the request')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $file = array_values($files)[0];
        if ($file->getError() !== 0) {
            return $this->ResponseBuilder
                ->error()
                ->message('Error happend while loading image')
                ->code(Errors::INVALID_DATA) // TODO: Change
                ->build();
        }

        // Less than 134B or bigger than 10MB
        if ($file->getSize() <= 134 || $file->getSize() >= 1024 * 1024 * 10) {
            return $this->ResponseBuilder
                ->error()
                ->message('Uploaded file size too small or big')
                ->code(Errors::IMAGE_SIZE)
                ->build();
        }

        if ($file->getClientMediaType() !== 'image/png' && $file->getClientMediaType() !== 'image/jpeg') {
            return $this->ResponseBuilder
                ->error()
                ->message('Unsuported image type')
                ->code(Errors::IMAGE_UNSUPORTED)
                ->build();
        } 

        $product = $this->Mongo->products->findOne([
            '_id' => new \MongoDB\BSON\ObjectId($productId)
        ]);

        if (!$product || !is_dir($this->StorageDir.'/'.$productId)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Pointed product does not exists')
                ->code(Errors::INVALID_DATA)
                ->build();
        }
        
        $fileExtension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        $file->moveTo($this->StorageDir.'/'.$productId.'/'."$slot.$fileExtension");

        return $this->ResponseBuilder
            ->success()
            ->build();

        
    }
}