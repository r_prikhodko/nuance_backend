<?php

namespace Lingua\Handlers\Auth;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use Lingua\Utils\Normalizer;
use Lingua\Utils\TokenBuilder;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class Login implements RequestHandlerInterface
{
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;
    
    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /**
     * @Inject 
     * @var TokenBuilder 
     * */
    private $TokenBuilder;

    /**
     *  @Inject
     *  @var Normalizer 
    */
    private $Normalizer;

    /**
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle (
            ServerRequestInterface $request
        ) : ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('email', $this->ValidationPresetsBuilder->email())
                ->key('password', $this->ValidationPresetsBuilder->password());

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $email = $this->Normalizer->email($data['email']);
        $password = $data['password'];

        $user = $this->Mongo->users->findOne(["email" => $email]);
        if (!$user) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::ACCOUNT_NOT_REGISTERED)
                ->message('Account with pointed email or password is not registered')
                ->build();
        }

        if (!password_verify($password, $user->password)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::ACCOUNT_NOT_REGISTERED)
                ->message('Account with pointed email or password is not registered')
                ->build();
        }

        $refreshToken = $this->TokenBuilder->buildRefreshToken();
        $accessToken = $this->TokenBuilder->buildAccessToken();
        $refreshTokenExpires = $this->TokenBuilder->generateRefreshTokenExpiresDate();
        $accessTokenExpires = $this->TokenBuilder->generateAccessTokenExpiresDate();
        $clientIp = $request->getAttribute('client-ip');
        $clientAgent = $request->getAttribute('client-agent');

        $this->Mongo->users->updateOne(
            ['_id' => $user->_id],
            ['$push' => [
                'tokens' => [
                    '$each' => [
                        [
                            'hash' => $refreshToken,
                            'ip' => $clientIp,
                            'browser' => $clientAgent,
                            'createdTime' => time(),
                            'expires' => $refreshTokenExpires,
                            'access' => [
                                'hash' => $accessToken,
                                'expires' => $accessTokenExpires,
                                'createdTime' => time()
                            ]
                        ]
                    ]
                ]
            ]]
        );

        return $this->ResponseBuilder
            ->success()
            ->data([
                'name' => $user->name,
                'permissions' => $user->permissions,
                'refreshToken' => $user->_id.'.'.$refreshToken,
                'accessToken' => $user->_id.'.'.$accessToken,
                'refreshTokenExpires' => $refreshTokenExpires,
                'accessTokenExpires' => $accessTokenExpires
            ])
            ->build();
    }
}