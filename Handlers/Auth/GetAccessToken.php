<?php

namespace Lingua\Handlers\Auth;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use Lingua\Utils\TokenBuilder;
use Lingua\Utils\GeoLocation;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class GetAccessToken implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /**
     * @Inject
     * @var GeoLocation
     */
    private $GeoLocation;

    /**
     * @Inject 
     * @var TokenBuilder 
     * */
    private $TokenBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key ('refreshToken', $this->ValidationPresetsBuilder->refreshToken());
        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $explodedRefreshToken = explode('.', $data['refreshToken']);
        $userId = $explodedRefreshToken[0];
        $refreshToken = $explodedRefreshToken[1];

        $user = $this->Mongo->users->findOne(["_id" => new \MongoDB\BSON\ObjectId($userId)]);
        if (!$user) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Pointed token was not found')
                ->build();
        }

        if (!isset($user->tokens)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Pointed token was not found')
                ->build();
        }

        // Try to find this token in an array
        $tokens = $user->tokens;
        $clientAgent = $request->getAttribute('client-agent');
        $clientIp = $request->getAttribute('client-ip');

        $foundTokenKey = $this->searchTokenInArrayObject($refreshToken, $tokens);
        if ($foundTokenKey === false) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Pointed token was not found')
                ->build();
        }

        $foundToken = $tokens[$foundTokenKey];
        if (time() >= $foundToken->expires) {
            $tokens->offsetUnset($foundTokenKey);
            $this->updateUserInDb($user, $userId);
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_EXPIRED)
                ->message('Pointed token has been expired')
                ->build();
        }

        // Token compromised
        if ($foundToken->access->expires >= time()) {
            $tokens->offsetUnset($foundTokenKey);
            $this->updateUserInDb($user, $userId);
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_COMPROMISED)
                ->message('Token compromised')
                ->build();
        }
        
        // The trust points is some kind of risk checker
        // Every weird action takes some points
        // * Different OS name - 20 points (OS X change names as well, as windows)
        // * Different OS family - 40 points (Critical)
        // * Different browser name - 30 points (Might be a bug, but it is crittical)
        // * Different browser version - 0 points
        // * Different ip - 10 points (It changes from time to time)
        // * Different city - 20 points (From time to time it's happening (while travelling))
        // * Different country - 35 points (Rare case)

        $trustPoints = 100;
        if ($foundToken->ip !== $clientIp) {
            $lastGeo = $this->GeoLocation->get($token->ip);
            $currentGeo = $this->GeoLocation->get($clientIp);
            $trustPoints -= 10;
            
            if ($currentGeo['city'] !== $lastGeo['city']) {
                $trustPoints -= 20;
            }

            if ($currentGeo['countryCode'] !== $lastGeo['countryCode']) {
                $trustPoints -= 35;
            }
        }

        if ($foundToken->browser !== $clientAgent) {
            $lastBrowser = new BrowserParser($foundToken->browser);
            $currentBrowser = new BrowserParser($clientAgent);

            if ($lastBrowser->browser->name !== $currentBrowser->browser->name) {
                $trustPoints -= 30;
            }

            if ($lastBrowser->os->family !== $currentBrowser->os->family) {
                $trustPoints -= 40;
            }

            if ($lastBrowser->os->name !== $currentBrowser->os->name) {
                $trustPoints -= 20;
            }
        }

        if ($trustPoints < 50) {
            $tokens[$foundTokenKey];
            $this->updateTokensInDb($tokens, $userId);
        } 

        $foundToken->access->hash = $this->TokenBuilder->buildAccessToken();
        $foundToken->access->expires = $this->TokenBuilder->generateAccessTokenExpiresDate();
        $foundToken->access->createdTime = time();
        $this->updateUserInDb($user, $userId);

        return $this->ResponseBuilder
                ->success()
                ->data([
                    'accessToken' => $userId.'.'.$foundToken->access->hash,
                    'accessTokenExpires' => $foundToken->access->expires
                ])
                ->build();
    }

    /**
     * Searches token in ArrayObject of tokens.
     * @param string $hash
     * @param \ArrayObject $tokens 
     * @return int|false
     */
    private function searchTokenInArrayObject(string $hash, \ArrayObject $tokens)
    {
        for ($key=0; $key < $tokens->count(); $key++) {
            $value = $tokens[$key];
            if ($value->hash === $hash) {
                return $key;
            }
        } return false;
    }

    /**
     * Updates user data in the db.
     * @param \ArrayObject $user
     * @param string $userId
     * @return mixed
     */
    private function updateUserInDb(\ArrayObject $user, string $userId) 
    {
        return $this->Mongo->users->updateOne(["_id" => new \MongoDB\BSON\ObjectId($userId)], [
            '$set' => $user
        ]);
    }

}