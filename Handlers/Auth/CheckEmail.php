<?php

namespace Lingua\Handlers\Auth;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use Lingua\Utils\Normalizer;
use Psr\Http\Message\ResponseInterface;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

// TODO: Remove repeatable code (CheckEmail \ CheckPhone)
class CheckEmail implements RequestHandlerInterface
{
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     */
    private $ValidationPresetsBuilder;
    
    /** 
     * @Inject
     * @var ResponseBuilder 
     */
    private $ResponseBuilder;
    
    /**
     * @Inject 
     * @var Normalizer 
     */
    private $Normalizer;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key (
            'email',
            $this->ValidationPresetsBuilder->email()
        );

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $found = $this->Mongo->users->count(["email" => $data['email']]);
        return $this->ResponseBuilder
            ->success()
            ->data(['exists' => (bool)$found])
            ->build();
    }

}