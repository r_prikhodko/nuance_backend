<?php

namespace Lingua\Handlers\Order;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MakeOrder implements RequestHandlerInterface
{
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     */
    private $ValidationPresetsBuilder;
    
    /** 
     * @Inject
     * @var ResponseBuilder 
     */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("url")
     */
    private $Url;

    /**
     * @Inject("botEmail")
     */
    private $BotEmail;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('name', v::stringType()->length(1,128))
                ->key('phone', v::stringType()->length(1,40))
                ->key('data', v::arrayVal()
                    ->each (
                        v::arrayVal()
                            ->key('id', $this->ValidationPresetsBuilder->mongoidString())
                            ->key('title', v::stringType()->length(1,128))
                            ->key('amount', v::intVal())
                    )->length(1,128)
                );

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $body = '';
        foreach($data['data'] as $element) {
            $body .= "<a href='".$this->Url."/product/".$element['id']."'> ".$element['title']." : ".$element['amount']." </a><br>";
        }
        
        $mailer = new PHPMailer(true);
        $mailer->isHTML(true);
        $mailer->setFrom($this->BotEmail, 'Bot');
        $mailer->addAddress('nuance_kz@mail.ru');
        $mailer->addAddress('r101.samp@yandex.ru');
        $mailer->CharSet = "UTF-8";
        $mailer->Subject = "Заказ";
        $mailer->Body = "
            <html>
                <h2>Имя: ".$data['name']."</h2>
                <h2>Телефон:".$data['phone']."</h2>
                $body 
            </html>
        ";
        $mailer->send();

        return $this->ResponseBuilder
            ->success()
            ->build();
    }
}