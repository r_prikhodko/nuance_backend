<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 9/4/18
 * Time: 4:20 AM
 */

namespace Lingua\Handlers\Account;

use Lingua\Handlers\Errors;
use Lingua\ResponseBuilder;
use Lingua\Utils\TokenBuilder;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Data\Mappers\User as UserMapper;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class ChangePassword implements RequestHandlerInterface
{
    /** @var ValidationPresetsBuilder  */
    private $ValidationPresetsBuilder;
    /** @var ResponseBuilder */
    private $ResponseBuilder;
    /** @var TokenBuilder */
    private $TokenBuilder;
    /** @var UserMapper */
    private $UserMapper;

    public function __construct (
        ValidationPresetsBuilder $presetsBuilder,
        ResponseBuilder $responseBuilder,
        TokenBuilder $tokenBuilder,
        UserMapper $userMapper
    ) {
        $this->ValidationPresetsBuilder = $presetsBuilder;
        $this->ResponseBuilder = $responseBuilder;
        $this->TokenBuilder = $tokenBuilder;
        $this->UserMapper = $userMapper;
    }

    public function handle (
            ServerRequestInterface $request
        ) : ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('token', $this->ValidationPresetsBuilder->token())
            ->key('id', $this->ValidationPresetsBuilder->id())
            ->key('newPassword', $this->ValidationPresetsBuilder->password())
            ->key('oldPassword', $this->ValidationPresetsBuilder->password());

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $this->UserMapper->setActiveFields(['id', 'password', 'token_hash', 'token_expires']);
        $user = $this->UserMapper->findById($data['id']);

        if (!$user || !$user->verifyToken($data['token'])) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid token')
                ->build();
        }

        if (!$user->isTokenActive()) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_EXPIRED)
                ->message('Token expired')
                ->build();
        }

        if (!$user->verifyPassword($data['oldPassword'])) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::OLD_PASSWORD_INCORRECT)
                ->message('Incorrect old password')
                ->build();
        }

        $user->setPasswordHash($data['newPassword']);
        $this->UserMapper->update($user);

        return $this->ResponseBuilder
            ->success()
            ->build();

    }
}