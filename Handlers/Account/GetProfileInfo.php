<?php

namespace Lingua\Handlers\Account;

use Lingua\Handlers\Errors;
use Respect\Validation\Validator as v;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Lingua\Data\Models\User as UserModel;
use Lingua\Data\Mappers\User as UserMapper;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\TokenBuilder;
use Lingua\ResponseBuilder;

class GetProfileInfo implements RequestHandlerInterface
{
    private $ValidationPresetsBuilder;
    private $ResponseBuilder;
    private $TokenBuilder;
    private $UserMapper;

    public function __construct (
        ValidationPresetsBuilder $presetsBuilder,
        ResponseBuilder $responseBuilder,
        TokenBuilder $tokenBuilder,
        UserMapper $userMapper
    ) {
        $this->ValidationPresetsBuilder = $presetsBuilder;
        $this->ResponseBuilder = $responseBuilder;
        $this->TokenBuilder = $tokenBuilder;
        $this->UserMapper = $userMapper;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $fieldsAvailable = ['email', 'name', 'surname', 'gender', 'rating', 'money'];
        $rule = v::key('id', $this->ValidationPresetsBuilder->id())
            ->key('token', $this->ValidationPresetsBuilder->token())
            ->key('fields', v::arrayType());

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->code(Errors::INVALID_DATA)
                ->message('Invalid user\'s data')
                ->build();
        }

        $fieldsRequired = array_intersect($data['fields'], $fieldsAvailable);
        if (!count($fieldsRequired)) {
            return $this->ResponseBuilder
                ->code(0)
                ->message('Not enough data')
                ->build();
        }

        $fullToken = $this->TokenBuilder
            ->buildFullToken(
                $data['token'],
                $request->getAttribute('client-ip')
            );

        $this->UserMapper->setActiveFields (
            array_merge(
                $fieldsRequired,
                ['tokenHash', 'tokenExpires']
            )
        );
        $user = $this->UserMapper->findById($data['id']);

        if (
            !$user ||
            !$user->verifyToken($fullToken)
        ) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid token or id')
                ->build();
        }

        if (!$user->isTokenActive()) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_EXPIRED)
                ->message('Token has expired')
                ->build();
        }

        return $this->ResponseBuilder
            ->success()
            ->data($this->mapModelToRequiredField (
                $user,
                $fieldsRequired
            ))
            ->build();
    }

    private function mapModelToRequiredField (
            UserModel $userModel,
            array $requiredFields
        ) : array
    {
        $userData = (array)$userModel;
        return array_intersect_key (
            $userData,
            array_flip($requiredFields)
        );
    }
}