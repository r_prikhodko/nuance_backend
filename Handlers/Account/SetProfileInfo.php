<?php

namespace Lingua\Handlers\Account;

use Lingua\Handlers\Errors;
use Lingua\ResponseBuilder;
use Lingua\Utils\Normalizer;
use Lingua\Utils\TokenBuilder;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Lingua\Data\Mappers\User as UserMapper;
use Lingua\Data\Models\User as UserModel;
use Respect\Validation\Validator as v;

class SetProfileInfo implements RequestHandlerInterface
{
    /** @var ValidationPresetsBuilder  */
    private $ValidationPresetsBuilder;
    /** @var ResponseBuilder */
    private $ResponseBuilder;
    /** @var TokenBuilder */
    private $TokenBuilder;
    /** @var Normalizer  */
    private $Normalizer;
    /** @var UserMapper */
    private $UserMapper;

    public function __construct(
        ValidationPresetsBuilder $presetsBuilder,
        ResponseBuilder $responseBuilder,
        TokenBuilder $tokenBuilder,
        Normalizer $normalizer,
        UserMapper $userMapper
    ) {
        $this->ValidationPresetsBuilder = $presetsBuilder;
        $this->ResponseBuilder = $responseBuilder;
        $this->TokenBuilder = $tokenBuilder;
        $this->Normalizer = $normalizer;
        $this->UserMapper = $userMapper;
    }

    public function handle (
            ServerRequestInterface $request
        ) : ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('token', $this->ValidationPresetsBuilder->token())
            ->key('id', $this->ValidationPresetsBuilder->id())
            ->key (
                'data',
                v::key('name', $this->ValidationPresetsBuilder->name(), false)
                ->key('surname', $this->ValidationPresetsBuilder->surname(), false)
                ->key('gender', $this->ValidationPresetsBuilder->gender(), false)
            );

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Invalid data')
                ->build();
        }

        $fieldsAvailable = ['name', 'surname', 'gender'];
        $data['data'] = array_intersect_key (
            $data['data'],
            array_flip($fieldsAvailable)
        );

        if (!count($data['data'])) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::INVALID_DATA)
                ->message('Data array should not be empty')
                ->build();
        }

        // Verify user's token
        $this->UserMapper->setActiveFields(['tokenHash', 'tokenExpires']);
        $user = $this->UserMapper->findById($data['id']);

        $fullToken = $this->TokenBuilder->buildFullToken(
            $data['token'],
            $request->getAttribute('client-ip')
        );

        if (
            !$user ||
            !$user->verifyToken($fullToken)
        ) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid token or id')
                ->build();
        }

        if (!$user->isTokenActive()) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_EXPIRED)
                ->message('Token has expired')
                ->build();
        }

        // Update user's data
        $this->UserMapper->setActiveFields(
            array_keys($data['data'])
        );
        $user->id = $data['id'];
        $user = $this->mergeUserModelWithArray($user, $data['data']);
        $this->UserMapper->update($user);

        return $this->ResponseBuilder
            ->success()
            ->build();
    }

    /**
     * Merges UserModel with an array
     * @param UserModel $model
     * @param array $data
     * @return UserModel
     */
    private function mergeUserModelWithArray (
            UserModel $model,
            array $data
        ) : UserModel
    {
        foreach ($data as $key => $value) {
            if (!property_exists($model, $key)) {
                throw new \RuntimeException("Cannot assign a value. There is no '$key' property in a UserModel object");
            }

            $model->$key = $value;
        }

        return $model;
    }

}