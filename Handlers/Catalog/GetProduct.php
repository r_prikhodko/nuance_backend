<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class GetProduct implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("storageDir")
     */
    private $StorageDir;

    /**
     * @Inject
     * @var Filesystem
     */
    private $Filesystem;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $auth = $request->getAttribute('client-auth');
        $rule = v::key('productId', $this->ValidationPresetsBuilder->mongoidString());
        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Inavlid productId')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $result = $this->Mongo->products->findOne(
            ['_id' => new \MongoDB\BSON\ObjectId($data['productId'])]
        );

        if (!$result) {
            return $this->ResponseBuilder
                ->error()
                ->message('Pointed product was not found')
                ->code(Errors::PRODUCT_NOT_FOUND)
                ->build();
        }

        // TODO: Check if user have enough permissions to see the draft
        //if ($result->published === false) {
        //    if (!$auth || !is_array($auth) || $auth['success'] !== true) {
        //        return $this->ResponseBuilder
        //            ->error()
        //            ->message('Pointed product is not available')
        //            ->code(Errors::PRODUCT_NOT_FOUND)
        //            ->build();
        //    }
        //}

        $output = $result->getArrayCopy();
        unset($output['_id']);
        if (isset($output['categoryId'])) {
            $output['categoryId'] = (string)$output['categoryId']; 
        }

        if (is_dir($this->StorageDir.'/'.$data['productId'])) {
            $output['imagesCount'] = $this->Filesystem->countDir($this->StorageDir.'/'.$data['productId']); 
        }
        
        return $this->ResponseBuilder
            ->success()
            ->data($output)
            ->build();

    }
}