<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class GetProductsList implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('search', $this->ValidationPresetsBuilder->searchString(), false)
                ->key('categoryId', $this->ValidationPresetsBuilder->mongoidString(), false)
                ->key('sort', $this->ValidationPresetsBuilder->sort(), false)
                ->key('skip', v::intType(), false);

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Inavlid productId')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $params = ['limit' => 20];
        if (isset($data['sort'])) {
            $sort = $data['sort'] === 'up' ? 1 : -1;
            $params['sort'] = ['price' => $sort];
        }

        if (isset($data['skip'])) {
            $params['skip'] = $data['skip'];
        }

        $result = null;
        if (isset($data['categoryId'])) {
            $result = $this->Mongo->products->find([
                '$and'=>[
                    ['categoryId' => new \MongoDB\BSON\ObjectId($data['categoryId'])],
                    ['published' => true]
                ]
            ], $params);
        } 
        else if (isset($data['search'])) {
            $result = $this->Mongo->products->find([
                '$and' => [
                    ['$text' => ['$search' => $data['search']]],
                    ['published' => true]
                ]
            ], $params);
        } 
        else {
            $result = $this->Mongo->products->find(
                ['published' => true], $params
            );
        }

        $output = $this->dbResponseToOutput($result->toArray());

        return $this->ResponseBuilder
            ->success()
            ->data($output)
            ->build();
    }

    private function dbResponseToOutput(array $result) : array 
    {
        return array_map(function($product) {
            $product['_id'] = (string)$product['_id'];
            if (isset($product['categoryId'])) {
                $product['categoryId'] = (string)$product['categoryId'];
            }

            return $product;
        }, $result);
    }
}