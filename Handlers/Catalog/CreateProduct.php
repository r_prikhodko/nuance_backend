<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class CreateProduct implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("storageDir")
     */
    private $StorageDir;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /*
        database layout:
            {
                _id - MongoId,
                title - string,
                categoryId - MongoId of category,
                parameters - array of strings,
                price - bigint,
                createdTime - unix timestamp,
                images - array of strings with fullname(filename and extension) of images,
                published - boolean (is product available for customers or not)
            }
        */

        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        // TODO: Check for permissions

        // Create an empry document in products collection
        $insertedDocument = $this->Mongo->products->insertOne([
            'published' => false,
            'createdTime' => time()
        ]);

        if (!$insertedDocument->isAcknowledged()) {
            throw new \Exception('Cannot insert product, db error');
        }

        // Create a image holding folder for this product
        if (!mkdir($this->StorageDir.'/'.$insertedDocument->getInsertedId())) {
            throw new \Exception('Cannot create folder');
        }

        return $this->ResponseBuilder
            ->success()
            ->data(['productId' => (string)$insertedDocument->getInsertedId()])
            ->build();
    }
}