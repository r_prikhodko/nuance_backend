<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class EditCategory implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $auth = $request->getAttribute('client-auth');
        $rule = v::key('categoryId', $this->ValidationPresetsBuilder->mongoidString())
                 ->key(
                    'data',
                    v::arrayType()->length(1, 2)
                     ->key('title', $this->ValidationPresetsBuilder->categoryTitle(), false)
                     ->key('parentId', v::oneOf(
                            $this->ValidationPresetsBuilder->mongoidString(),
                            v::nullType()
                     ), false)
                );

        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Inavlid data')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        if (isset($data['data']['parentId'])) {
            $data['data']['parentId'] = new \MongoDB\BSON\ObjectId($data['data']['parentId']);
            $searchResult = $this->Mongo->categories->findOne([
                '_id' => $data['data']['parentId']
            ]);

            if (!$searchResult) {
                return $this->ResponseBuilder
                    ->error()
                    ->message('Invalid parent id')
                    ->code(Errors::INVALID_DATA)
                    ->build();
            }

            // Prevent Circular references
            if ((string)$searchResult->parentId === (string)$data['categoryId']) {
                return $this->ResponseBuilder
                    ->error()
                    ->message('Circular reference')
                    ->code(Errors::INVALID_DATA)
                    ->build();
            }
        }

        $result = $this->Mongo->categories->updateOne(
            ['_id' => new \MongoDB\BSON\ObjectId($data['categoryId'])],
            ['$set'=>$data['data']]
        );

        if (!$result) {
            return $this->ResponseBuilder
                ->error()
                ->message('Pointed category was not found')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

       return $this->ResponseBuilder
            ->success()
            ->build();
    }
}