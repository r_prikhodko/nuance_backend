<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class EditProduct implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /*
            {
                productId: id,
                data: {
                    title: string,
                    categoryId: MongoId of category,
                    parameters: arary of strings,
                    price: bigint,
                    published: boolean
                }
            }
        */

        $data = $request->getAttribute('json-data');
        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        // TODO: Check for permissions
        
        $rule = v::key('productId', $this->ValidationPresetsBuilder->mongoidString())
            ->key (
                'data',
                v::key('title', $this->ValidationPresetsBuilder->productTitle(), false)
                ->key('categoryId', $this->ValidationPresetsBuilder->mongoidString(), false)
                ->key('params', $this->ValidationPresetsBuilder->productParameters(), false)
                ->key('price', v::intType(), false)
                ->key('published', v::boolType(), false)
            );

        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Invalid data')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        if (isset($data['data']['categoryId'])) {
            $data['data']['categoryId'] = new \MongoDB\BSON\ObjectId($data['data']['categoryId']);
        }

        $result = $this->Mongo->products->updateOne(
            ['_id' => new \MongoDB\BSON\ObjectId($data['productId'])],
            ['$set' => $data['data']]
        );

        return $this->ResponseBuilder
            ->success()
            ->build();
    }
}