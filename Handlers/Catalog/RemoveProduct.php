<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class RemoveProduct implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject
     * @var Filesystem
     */
    private $Filesystem;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    /**
     * @Inject("storageDir")
     */
    private $StorageDir;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        // TODO: Check for permissions

        $rule = v::key('productId', $this->ValidationPresetsBuilder->mongoidString());
        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Inavlid productId')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $result = $this->Mongo->products->deleteOne(['_id' => new \MongoDB\BSON\ObjectId($data['productId'])]);
        if ($result->getDeletedCount() === 0) {
            return $this->ResponseBuilder
                ->error()
                ->message('Product with pointed id does not exist')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $this->Filesystem->delete($this->StorageDir.'/'.$data['productId']);
        return $this->ResponseBuilder
            ->success()
            ->build();
    }
}