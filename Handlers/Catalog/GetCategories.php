<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Lingua\Utils\Filesystem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class GetCategories implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $data = $request->getAttribute('json-data');
        $rule = v::key('topLevel', v::boolType(), false)
                ->key('parentId', $this->ValidationPresetsBuilder->mongoidString(), false);
            
        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Inavlid data')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        $result = null;
        if (!$data) {
            $result = $this->Mongo->categories->find();
        } else if (isset($data['topLevel'])) {
            $result = $this->Mongo->categories->find(['parentId' => null]);
        } else if (isset($data['parentId'])) {
            $result = $this->Mongo->categories->find([
                'parentId' => new \MongoDB\BSON\ObjectId($data['parentId'])
            ]);
        }

        if (!$result) {
            return $this->ReponseBuilder
                ->success()
                ->data([])
                ->build();
        }

        $output = array_map(function($category){
            $category['_id'] = (string)$category['_id'];
            if ($category['parentId']) {
                $category['parentId'] = (string)$category['parentId'];
            }

            return $category;
        }, $result->toArray());

        return $this->ResponseBuilder
            ->success()
            ->data($output)
            ->build();
    }
}