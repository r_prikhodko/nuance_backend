<?php

namespace Lingua\Handlers\Catalog;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use WhichBrowser\Parser as BrowserParser;
use Lingua\Utils\ValidationPresetsBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Respect\Validation\Validator as v;

class CreateCategory implements RequestHandlerInterface
{   
    /** 
     * @Inject
     * @var ValidationPresetsBuilder 
     * */
    private $ValidationPresetsBuilder;

    /**
     * @Inject 
     * @var ResponseBuilder 
     * */
    private $ResponseBuilder;
    
    /** 
     * @Inject("Mongo")
     */
    private $Mongo;

    public function handle(ServerRequestInterface $request): ResponseInterface
    {   
        $data = $request->getAttribute('json-data');
        $auth = $request->getAttribute('client-auth');
        if (!$auth || !is_array($auth) || $auth['success'] !== true) {
            return $this->ResponseBuilder
                ->error()
                ->message('Auth failed, please send Authentication')
                ->code(Errors::AUTH_FAILED)
                ->build();
        }

        $rule = v::key('title', $this->ValidationPresetsBuilder->categoryTitle())
            ->key('parentId', $this->ValidationPresetsBuilder->mongoidString(), false);
        
        if (!$rule->validate($data)) {
            return $this->ResponseBuilder
                ->error()
                ->message('Invalid data')
                ->code(Errors::INVALID_DATA)
                ->build();
        }

        // TODO: Check for permissions

        // Check is parent category exists and valid
        if (isset($data['parentId'])) {
            $searchResult = $this->Mongo->categories->findOne([
                '_id' => new \MongoDB\BSON\ObjectId($data['parentId'])
            ]);
    
            // It is possible to add multilevel categories (posability for the future use)
            if (!$searchResult) {
                return $this->ResponseBuilder
                    ->error()
                    ->message('Invalid parent id')
                    ->code(Errors::INVALID_DATA)
                    ->build();
            }
        }

        // Create an empry document in products collection
        $insertedDocument = $this->Mongo->categories->insertOne([
            'title' => $data['title'],
            'parentId' => isset($data['parentId']) 
                        ? new \MongoDB\BSON\ObjectId($data['parentId'])
                        : null
        ]);

        if (!$insertedDocument->isAcknowledged()) {
            throw new \Exception('Cannot insert product, db error');
        }

        return $this->ResponseBuilder
            ->success()
            ->data(['categoryId' => (string)$insertedDocument->getInsertedId()])
            ->build();
    }
}