<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 7/12/18
 * Time: 11:13 AM
 */

namespace Lingua\Middlewares;

use Lingua\Errors;
use Lingua\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Authentication implements MiddlewareInterface {

    /**
     * @Inject
     * @var ResponseBuilder
     */
    private $ResponseBuilder;

    /**
     * @Inject("Mongo")
     */
    private $Mongo;

    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) : ResponseInterface
    {
        // By default client-auth is false
        $request = $request->withAttribute('client-auth', false);
        $clientAgent = $request->getAttribute('client-agent');
        $clientIp = $request->getAttribute('client-ip');

        // Check is header present
        if (!$request->hasHeader("Authentication")) {
            return $handler->handle($request);
        }

        // Parse the header and check validity
        $auth = $request->getHeader("Authentication");
        if (!isset($auth[0])) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid access token format')
                ->build();
        }

        $explodedAuth = explode(" ", $auth[0]);
        if (count($explodedAuth) !== 2 || $explodedAuth[0] !== 'token') {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid access token format')
                ->build();
        }

        // User-side token consist of user id and token so divide it to userId and userToken
        $explodedToken = explode(".", $explodedAuth[1]);
        if (count($explodedToken) !== 2) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Invalid access token format')
                ->build();
        }

        $userId = $explodedToken[0];
        $userToken = $explodedToken[1];

        // Check is user with pointed id exists
        $user = $this->Mongo->users->findOne(["_id" => new \MongoDB\BSON\ObjectId($userId)]);
        if (!$user) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Pointed token was not found')
                ->build();
        }

        $tokens = $user->tokens;
        $foundTokenKey = $this->searchAccessTokenInArrayObject($userToken, $tokens);
        if ($foundTokenKey === false) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_INVALID)
                ->message('Token was not found')
                ->build();
        }

        $foundToken = $user->tokens[$foundTokenKey];
        if ($foundToken->access->expires <= time()) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_EXPIRED)
                ->message('Token has been expired')
                ->build();
        }

        if ($clientAgent !== $foundToken->browser) {
            return $this->ResponseBuilder
                ->error()
                ->code(Errors::TOKEN_COMPROMISED)
                ->message('Token was not found')
                ->build();
        }

        $request = $request->withAttribute (
            'client-auth', 
            [
                'id' => $userId,
                'token' => $userToken,
                'permissions' => $user->permissions,
                'success' => true
            ]
        );

        return $handler->handle($request);
    }

    /**
     * Searches access token in ArrayObject of tokens.
     * @param string $hash
     * @param \ArrayObject $tokens 
     * @return int|false
     */
    private function searchAccessTokenInArrayObject(string $hash, \ArrayObject $tokens)
    {
        for ($key=0; $key < $tokens->count(); $key++) {
            $value = $tokens[$key];
            if ($value->access->hash === $hash) {
                return $key;
            }
        } return false;
    }
}