<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 7/15/18
 * Time: 7:53 AM
 */

namespace Lingua\Middlewares;

use Lingua\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ClientIp implements MiddlewareInterface
{
    /** @var ResponseBuilder */
    private $ResponseBuilder;

    public function __construct (
        ResponseBuilder $responseBuilder
    ) {
        $this->ResponseBuilder = $responseBuilder;
    }

    public function process (
            ServerRequestInterface $request,
            RequestHandlerInterface $handler
        ) : ResponseInterface
    {
        $params = $request->getServerParams();
        if (isset($params['LINGUA_CLIENT_IPV4'])) {
            $request = $request->withAttribute (
                'client-ip',
                '::ffff:'.$params['LINGUA_CLIENT_IPV4']
            );
        } else if (isset($params['LINGUA_CLIENT_IPV6'])) {
            $request = $request->withAttribute (
                'client-ip',
                $params['LINGUA_CLIENT_IPV6']
            );
        } else {
            return $this->ResponseBuilder
                ->error()
                ->code(0)
                ->message('Client\'s ip is not available')
                ->build();
        }

        return $handler->handle($request);
    }
}