<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 7/15/18
 * Time: 7:53 AM
 */

namespace Lingua\Middlewares;

use Lingua\ResponseBuilder;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ClientAgent implements MiddlewareInterface
{
    /** @var ResponseBuilder */
    private $ResponseBuilder;

    public function __construct (
        ResponseBuilder $responseBuilder
    ) {
        $this->ResponseBuilder = $responseBuilder;
    }

    public function process (
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ) : ResponseInterface
    {
        $params = $request->getServerParams();
        $clientAgent = isset($params['HTTP_USER_AGENT'])
            ? $params['HTTP_USER_AGENT']
            : 'unknown';

        return $handler->handle (
            $request->withAttribute(
                'client-agent',
                $clientAgent
            )
        );
    }
}