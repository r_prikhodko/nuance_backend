#! /bin/bash

echo Name:
read name
echo Phone:
read phone
echo Title:
read title
echo Id:
read id
echo Amount:
read amount

curl \
-H 'Content-Type: application/json' \
-d "{\"name\":\"$name\", \"phone\":\"$phone\", \"data\":[{\"title\":\"$title\", \"amount\":\"$amount\", \"id\":\"$id\"}]}" \
localhost/api/order/make