#! /bin/bash

echo Write your access token
read accessToken
echo Write title
read title

curl \
-H "Authentication: token $accessToken" \
-H "Content-Type: application/json" \
-X POST \
-d "{\"title\":\"$title\"}" \
localhost/api/catalog/createCategory