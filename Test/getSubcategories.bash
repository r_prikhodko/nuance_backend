#! /bin/bash

echo Parent Id:
read parentId

curl \
-H "Content-Type: application/json" \
-X POST \
-d "{\"parentId\":\"$parentId\"}" \
localhost/api/catalog/getCategories