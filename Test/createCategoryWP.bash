#! /bin/bash

echo Write your access token
read accessToken
echo Write title
read title
echo Parent Id
read parentId

curl \
-H "Authentication: token $accessToken" \
-H "Content-Type: application/json" \
-X POST \
-d "{\"title\":\"$title\", \"parentId\":\"$parentId\"}" \
localhost/api/catalog/createCategory

# 5d25c562dd50b901905ef2b5