#! /bin/bash

echo Write your access token
read accessToken
curl \
-H "Authentication: token $accessToken" \
localhost/api/test/auth

# RefreshToken: 5d39cba65c1afc779365245f.a5f163d3733f11fced933af16ea89fbfce3eb0fe0b18d8df5178d86214c3bd03
# AccessToken: 5d39cba65c1afc779365245f.87018d5d34cbeee683e5474a1588cccb7490ec21b65e05fdd1964bee01d21af0