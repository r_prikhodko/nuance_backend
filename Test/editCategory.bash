#! /bin/bash

echo Access token:
read accessToken
echo CategoryId:
read categoryId
#echo Title:
#read title
echo Parent Id:
read parentId

curl \
-H "Authentication: token $accessToken" \
-H "Content-Type: application/json" \
-X POST \
-d "{\"categoryId\":\"$categoryId\", \"data\":{\"parentId\":\"$parentId\"}}" \
localhost/api/catalog/editCategory

#\"title\":\"$title\",