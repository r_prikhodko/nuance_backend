#! /bin/bash

echo Product id:
read productId
curl \
-H 'Content-Type: application/json' \
-d "{\"productId\": \"$productId\"}" \
localhost/api/catalog/getProduct