echo Write refresh token:
read token
curl \
-H 'Content-Type: application/json' \
-d "{\"refreshToken\": \"$token\"}" \
localhost/api/auth/getAccessToken