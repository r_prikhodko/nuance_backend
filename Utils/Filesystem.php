<?php

namespace Lingua\Utils;

class Filesystem 
{
    function delete($target) 
    {
        if(is_dir($target)){
            $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
    
            foreach( $files as $file ){
                $this->delete( $file );
            }
    
            rmdir( $target );
        } elseif(is_file($target)) {
            unlink( $target );  
        }
    }

    function countDir($target) 
    {
        $fi = new \FilesystemIterator($target, \FilesystemIterator::SKIP_DOTS);
        return iterator_count($fi);
    }
}