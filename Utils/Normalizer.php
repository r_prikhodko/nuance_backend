<?php

namespace Lingua\Utils;

class Normalizer
{
    public function email(string $email) : string
    {
        $email = strtolower($email);     // lowercase it
        $email = strip_tags($email);     // Strip all html tags inside the Email string
        return htmlspecialchars($email); // XML security
    }

    public function name(string $name) : string
    {
        $name = strtolower($name);
        $name = ucfirst($name);
        $name = strip_tags($name);      // Strip all html tags inside the Email string
        return htmlspecialchars($name); // XML security
    }
}