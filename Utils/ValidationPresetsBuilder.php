<?php declare(strict_types=1);

namespace Lingua\Utils;

use Respect\Validation\Validator as v;

/**
 * Class Validator
 * Just simple wrapper around Respect\Validator.
 * @package Lingua\Utils
 */
class ValidationPresetsBuilder
{
    /**
     * Returns Validator for password.
     * @example validator->email()->validate('email');
     * @return v
     */
    public function password()
    { return v::stringType()->length(5, 128); }

    /**
     * Returns Validator for email address.
     * @example validator->email()->validate('email');
     * @return v
     */
    public function email()
    { return v::email(); }

    /**
     * Returns Validator for user's privileges
     * @return v
     */
    public function permissions()
    { return v::arrayType()->each(v::stringType()); }

    /**
     * Returns Validator for user's name
     * @return v
     */
    public function name()
    { return v::regex('/^[a-zA-Zа-яА-Я]{1,32}$/'); }

    /**
     * Returns Validator for refresh token
     * @return v
     */
    public function refreshToken()
    { return v::regex('/^.{20,128}\..{20,128}$/');}

    /**
     * Returns Validator for access token
     * @return v
     */
    public function accessToken()
    { return v::regex('/^.{20,128}\..{20,128}$/');}

    /**
     * Returns Validator for MogodbID.
     * @return v
     */
    public function mongoidString()
    { return v::stringType()->length(23,25); }

    /**
     * Returns Validator for product title.
     * @return v
     */
    public function productTitle()
    { return v::stringType()->length(1, 512); }

    /**
     * Returns Validator for product parameters.
     * @return v
     */
    public function productParameters()
    { return v::arrayType()->each(v::stringType()); }

    /**
     * Returns Validator for product title.
     * @return v
     */
    public function categoryTitle()
    { return v::stringType()->length(1, 128); }

    /**
     * Returns Validator for product title.
     * @return v
     */
    public function searchString()
    { return v::stringType()->length(1, 128); }

    /**
     * Returns Validator for product title.
     * @return v
     */
    public function sort()
    { 
        return v::oneOf (
            v::equals('up'),
            v::equals('down')
        );
    }
}
