<?php

namespace Lingua\Utils;

/**
 * Class TokenBuilder Builds different type of tokens
 * @package Lingua\Builders
 */
class TokenBuilder
{
    private const REFRESH_TOKEN_ALGO = 'sha256';
    private const ACCESS_TOKEN_ALGO = 'sha256';
    
    private const REFRESH_TOKEN_LIFETIME = 60 * 60 * 24 * 7;
    private const ACCESS_TOKEN_LIFETIME = 60 * 10;

    /**
     * Generates refresh token.
     * User may store this token into cookies
     * @return string refreshToken
     * @throws \Exception
     */
    public function buildRefreshToken() : string
    {
        return hash(self::REFRESH_TOKEN_ALGO, random_bytes(70));
    }

    /**
     * Generates refresh token.
     * Must not be saved somewhere in a browser. (Stores only in active memory - in variables)
     * @return string accessToken
     * @throws \Exception
     */
    public function buildAccessToken() : string
    {
        return hash(self::ACCESS_TOKEN_ALGO, random_bytes(50));
    }

    /**
     * Generates refresh token expires time (after when token is invalid).
     * @return int unixTimestamp
     */
    public function generateRefreshTokenExpiresDate() : int 
    {
        return time() + self::REFRESH_TOKEN_LIFETIME;
    }

    /**
     * Generates access token expires time (after when token is invalid).
     * @return int unixTimestamp
     * @throws \Exception
     */
    public function generateAccessTokenExpiresDate() : int 
    {
        return time() + self::ACCESS_TOKEN_LIFETIME;
    }

}