<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 7/18/18
 * Time: 5:59 AM
 */

namespace Lingua\Utils;

class StrManipulator
{
    /**
     * @param string $str
     * @return string
     */
    static public function CamelCaseToUnderscore(string $str)
    {
        $words = preg_split('/(?=[A-Z]+)/', $str);
        $underscoreStr = '';
        foreach($words as $key => $value) {
            $value = strtolower($value);
            if ($underscoreStr) {
                $value = '_'.$value;
            }

            $underscoreStr .= $value;
        }

        return $underscoreStr;
    }

    /**
     * @param string $str
     * @return string camel case string
     */
    static public function UnderscoreToCamelCase(string $str)
    {
        $str = strtolower($str);
        $words = explode('_', $str);
        $camelCaseStr = '';
        foreach($words as $key => $value) {
            if ($key !== 0) {
                $value = ucfirst($value);
            }

            $camelCaseStr .= $value;
        }

        return $camelCaseStr;
    }
}