<?php

namespace Lingua\Utils;

class GeoLocation
{
    public function get(string $ip) : array
    {
        $geopluginData = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
        return [
            'city' => $geopluginData['geoplugin_city'],
            'region' => $geopluginData['geoplugin_region'],
            'regionCode' => $geopluginData['geoplugin_regionCode'],
            'regionName' => $geopluginData['geoplugin_regionName'],
            'countryCode' => $geopluginData['geoplugin_countryCode'],
            'countryName' => $geopluginData['geoplugin_countryName'],
            'continentCode' => $geopluginData['geoplugin_continentCode'],
            'continentName' => $geopluginData['geoplugin_continentName']
        ];
    }
}