<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 7/12/18
 * Time: 12:26 PM
 */

namespace Lingua;

use function DI\get;
use function DI\create;
use Lingua\DeferredTasks\TaskBuilder;
use Lingua\DeferredTasks\TasksManager;

return [
    'storageDir' => '/var/nuance',
    'url' => 'http://nuance.kz',
    'botEmail' => 'bot@nuance.kz',
    TasksManager::class => create()
        ->constructor(
            get(TaskBuilder::class),
            get('expires.deferredTask')
        )
];