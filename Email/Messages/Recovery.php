<?php

namespace Lingua\Email\Messages;

use Lingua\Email\Message;

class Recovery extends Message
{
    public static function confirmation() : Recovery
    {
    }

    public static function success() : Recovery
    {

    }

    public function setLanguage(string $lang)
    {
        $this->Language = $lang;
    }

    public function getLanguage(): string
    {
        return $this->Language;
    }

    public function send(string $pTo)
    {
        $this->Subject = "Recovery";
        $this->FileName = "main.html";
        $this->FromName = "Support team";
        $this->To = $pTo;

        return $this->SendMail( $this->ParseTemplate($pData->toArray()) );
    }
}