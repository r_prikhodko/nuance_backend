<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11/1/17
 * Time: 1:49 AM
 */

namespace Lingua\Email\Messages;

use Lingua\Email\CommonData;
use Lingua\Email\Message;
use Lingua\Email\EmailData;

class SetData extends Message
{
    public function send(string $pTo, EmailData $pData = null) : bool {
        $this->Subject = "Service";
        $this->FileName = "main.html";
        $this->FromName = "Support team";
        $this->To = $pTo;

        return $this->SendMail( $this->ParseTemplate([]) );
    }
}