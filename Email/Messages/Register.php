<?php

namespace Lingua\Email\Messages;

use Lingua\Email\Message;
use Lingua\Utils\External\Client;

class Register extends Message
{
    public static function confirmation(string $code, int $id, string $language) : Register
    {
        $msg = new Register();
        $msg->Subject = "Service";
        $msg->FromName = "Support team";
        $msg->Body = $msg->ParseTemplate($language.'/main.html', ['code'=>$code,'id'=>$id]);
        return $msg;
    }

    public static function success(string $language) : Register
    {
        $msg = new Register();
        $msg->Subject = "Registration";
        $msg->FromName = "Lingua.space";
        $msg->Body = $msg->ParseTemplate($language.'/main.html', []);
        return $msg;
    }
}