<?php
/**
 * All abstract layouts that provide functions for work with Emails.
 * There are several classes here
 * @defgroup Mail Mail
 */

namespace Lingua\Email;

use Lingua\Exceptions\BasicException;
use Lingua\Email\EmailData;
use Lingua\Exceptions\ServerException;

abstract class Message
{
    // You should setting up the following attributes from derived classes
    protected $From = "no-replay@lingua.space"; // Message sender's address
    protected $FromName = "";   // Message sender's name
    protected $Subject = "";    // Subject of the message
    protected $Body = "";       // Body of the message

    /**
     * Sends the email to the receiver
     * @param string $to
     */
    public function send(string $to)
    {
        try {
            $mailer = new \PHPMailer(true);
            $mailer->addAddress($to);
            $mailer->isHTML(true);
            $mailer->CharSet = 'UTF-8';
            $mailer->From = $this->From;
            $mailer->Body = $this->Body;
            $mailer->FromName = $this->FromName;
            $mailer->Subject = $this->Subject;
            $mailer->send();
        } catch (\phpmailerException $e) {
            throw new ServerException("Cannot send email: ", ServerException::CODE_INTERNAL);
        }
    }

    /**
     * Parses html templates
     * @param string $templateName
     * @param array $valuesArray
     * @return string
     */
    protected function ParseTemplate(string $templateName, array $valuesArray): string
    {
        $path = realpath(dirname(__FILE__)."/Templates/$templateName");
        $content = file_get_contents($path);
        foreach ($valuesArray as $key => $value) {
            $content = str_replace('{{' . $key . '}}', $value, $content);
        }
        return $content;
    }
}