<?php
declare(strict_types=1);

namespace Lingua;

use function Http\Response\send;
use function FastRoute\simpleDispatcher;
use Relay\Relay;
use DI\ContainerBuilder;
use FastRoute\RouteCollector;
use Zend\Diactoros\ServerRequestFactory;
use Middlewares\RequestHandler;
use Middlewares\FastRoute;
use Lingua\Middlewares\ClientAgent;
use Lingua\Middlewares\ClientIp;
use Lingua\Middlewares\RequestParser;
use Lingua\Middlewares\Authentication;
use Lingua\Handlers\Auth\CheckEmail;
use Lingua\Handlers\Auth\Login;
use Lingua\Handlers\Auth\GetAccessToken;
use Lingua\Handlers\Catalog\CreateProduct;
use Lingua\Handlers\Catalog\RemoveProduct;
use Lingua\Handlers\Catalog\EditProduct;
use Lingua\Handlers\Catalog\GetProduct;
use Lingua\Handlers\Catalog\CreateCategory;
use Lingua\Handlers\Catalog\RemoveCategory;
use Lingua\Handlers\Catalog\EditCategory;
use Lingua\Handlers\Catalog\GetCategories;
use Lingua\Handlers\Catalog\GetProductsList;
use Lingua\Handlers\Media\LoadImage;
use Lingua\Handlers\Media\RemoveImage;
use Lingua\Handlers\Order\MakeOrder;

require 'vendor/autoload.php';

try {
    // Create and initialize the ServerRequest (PSR-7)
    $request = ServerRequestFactory::fromGlobals();

    // Set up the DIC (PSR-11)
    $containerBuilder = new ContainerBuilder();
    $containerBuilder->useAnnotations(true);
    $containerBuilder->addDefinitions('./Configurations/lifetime.php');
    $containerBuilder->addDefinitions('./Configurations/database.php');
    $containerBuilder->addDefinitions('./Configurations/request.php');
    $containerBuilder->addDefinitions('dependencies.php');
    $container = $containerBuilder->build();

    // Create an API
    $dispatcher = simpleDispatcher(function(RouteCollector $r) use ($container) {
        $r->addGroup('/catalog', function(RouteCollector $r) use ($container) {
            $r->addRoute(['POST'], '/createProduct', $container->get(CreateProduct::class)); // Works
            $r->addRoute(['POST'], '/removeProduct', $container->get(RemoveProduct::class)); // Works
            $r->addRoute(['POST'], '/editProduct', $container->get(EditProduct::class)); // Not tested
            $r->addRoute(['POST'], '/getProduct', $container->get(GetProduct::class)); // Works
            $r->addRoute(['GET', 'POST'], '/getProductsList', $container->get(GetProductsList::class)); // Testing

            $r->addRoute(['POST'], '/createCategory', $container->get(CreateCategory::class)); // Works
            $r->addRoute(['POST'], '/editCategory', $container->get(EditCategory::class)); // Works
            $r->addRoute(['POST'], '/removeCategory', $container->get(RemoveCategory::class)); // Works
            $r->addRoute(['POST'], '/getCategories', $container->get(GetCategories::class)); // Works            
        });

        $r->addGroup('/media', function(RouteCollector $r) use ($container) {
            $r->addRoute(['POST'], '/loadImage/{productId}/{slot:[0-9]}', $container->get(LoadImage::class)); // Works
            $r->addRoute(['POST'], '/removeImage/{productId}/{slot:[0-9]}', $container->get(RemoveImage::class)); // Works
        });

        $r->addGroup('/auth', function(RouteCollector $r) use ($container) {
            $r->addRoute(['POST'], '/login', $container->get(Login::class));
            $r->addRoute(['POST'], '/getAccessToken', $container->get(GetAccessToken::class));
            //$r->addRoute(['POST'], '/checkEmail', $container->get(CheckEmail::class));
        });

        $r->addGroup('/order', function(RouteCollector $r) use ($container) {
            $r->addRoute(['POST', 'GET'], '/make', $container->get(MakeOrder::class));
        });
        
        /* Temprary unavailable - maybe in the future
        $r->addGroup('/account', function(RouteCollector $r) use ($container) {
            $r->addRoute(['GET', 'POST'], '/setProfileInfo', $container->get(SetProfileInfo::class));
            $r->addRoute(['GET', 'POST'], '/getProfileInfo', $container->get(GetProfileInfo::class));
            $r->addRoute(['GET', 'POST'], '/changePassword', $container->get(ChangePassword::class));
        });*/
    });

    // Create a middleware pipe (PSR-15) | Warning! The order of middlewares makes sense
    $pipe[] = $container->get(ClientAgent::class);
    $pipe[] = $container->get(RequestParser::class);
    $pipe[] = $container->get(Authentication::class);
    $pipe[] = new FastRoute($dispatcher);
    $pipe[] = new RequestHandler($container);

    // Start processing the pipe
    $handler = new Relay($pipe);
    $response = $handler->handle($request);

    send($response);

} catch (\Exception $e) {
    send (
        (new ResponseBuilder)
        ->error()
        ->code(0x0001)
        ->message($e->getMessage()) // TODO: Log the message and NEVER show it to the user
        ->build()
    );
}
