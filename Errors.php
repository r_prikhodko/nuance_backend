<?php

namespace Lingua;

class Errors
{
    const AUTH_FAILED = 0x1000;
    const INVALID_DATA = 0x1001;
    const TOKEN_INVALID = 0x1002;
    const TOKEN_EXPIRED = 0x1003;
    const TOKEN_COMPROMISED = 0x1004;
    const ACCOUNT_NOT_ACTIVATED = 0x1005;
    const ACCOUNT_NOT_REGISTERED = 0x1006;
    const ACCOUNT_ALREADY_REGISTERED = 0x1007;
    const TASK_EXPIRED = 0x1008;
    const TASK_NOT_FOUND = 0x1009;
    const OLD_PASSWORD_INCORRECT = 0x1010;
    const PRODUCT_NOT_FOUND = 0x1011;
    const IMAGE_SIZE = 0x1012;
    const IMAGE_UNSUPORTED = 0x1013;
}